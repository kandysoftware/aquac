/**
 *
 */
grammar Aqua;

compilationUnit
    :   (declaration | expression)* EOF
    ;

declaration
    :   ACTOR Identifier typeAnnotation? ASSIGN expression  # Actor
    |   CONST Identifier typeAnnotation? ASSIGN expression  # Immutable
    |   LET Identifier typeAnnotation? ASSIGN expression    # Mutable
    |   LET? Identifier BECOME Identifier                   # Becomes
    ;

expression
    :   functionExpression                                  # Function
    |   blockExpression                                     # Block
    |   Identifier arguments                                # Call
    |   expression arguments                                # IIFE
    |   tupleExpression                                     # Tuple
    |   literalExpression                                   # Literal
    |   expression DOT Identifier                           # Accessor
    |   expression (INC | DEC)                              # Postfix
    |   (Sign | INC | DEC) expression                       # Prefix
    |   (TILDE | BANG) expression                           # Negation
    |   expression op=(MUL | DIV | MOD) expression          # Multiplicative
    |   expression op=(ADD | SUB) expression                # Additive
    |   expression op=(LE | GE | GT | LT) expression        # Relational
    |   expression op=(EQUAL | NOTEQUAL) expression         # Comparision
    |   expression BITAND expression                        # BitwiseAnd
    |   expression CARET expression                         # BitwiseXor
    |   expression BITOR expression                         # BitwiseOr
    |   expression AND expression                           # LogicalAnd
    |   expression OR expression                            # LogicalOr
    |   expression QUESTION expression COLON expression     # Ternary
    |   <assoc=right> expression
        (   ASSIGN
        |   ADD_ASSIGN
        |   SUB_ASSIGN
        |   MUL_ASSIGN
        |   DIV_ASSIGN
        |   AND_ASSIGN
        |   OR_ASSIGN
        |   XOR_ASSIGN
        |   RSHIFT_ASSIGN
        |   URSHIFT_ASSIGN
        |   LSHIFT_ASSIGN
        |   MOD_ASSIGN
        )
        expression                                          # Assignment
    |   Identifier                                          # Id
    ;

functionExpression
    :    formalParameters typeAnnotation? blockExpression
    ;

blockExpression
    :   LBRACE RBRACE                                       # EmptyBlock
    |   LBRACE (declaration | expression)* RBRACE           # NonemptyBlock
    ;

tupleExpression
    :   LPAREN RPAREN                                       # Unit
    |   LPAREN expression (COMMA expression)* RPAREN        # NTuple
    ;

literalExpression
    :   IntegerLiteral                                      # Integer
    |   CharacterLiteral                                    # Char
    |   StringLiteral                                       # String
    |   BooleanLiteral                                      # Boolean
    |   NOTHING                                             # Bottom
    ;

formalParameters
    :   LPAREN formalParameter (COMMA formalParameter)* RPAREN
    ;
formalParameter
    :   Identifier typeAnnotation?
    ;

arguments
    :   LPAREN argument (COMMA argument)* RPAREN
    ;

argument
    :   expression
    ;

typeAnnotation
    :   COLON type
    ;

type
    :   PLACEHOLDER
    |   primitiveType
    |   tupleType
    |   referenceType
    ;

typeList
    :   type (COMMA type)*
    ;

primitiveType
    :   INT                                                 # IntegerType
    |   DOUBLE                                              # DoubleType
    |   BOOLEAN                                             # BooleanType
    ;

tupleType
    :   LPAREN typeList RPAREN
    ;

referenceType
    :   Identifier                                          # Monomorphic
    ;

// LEXER

// Keywords

ACTOR         : 'actor';
BOOLEAN       : 'boolean';
BECOME        : 'become';
CASE          : 'case';
CONST         : 'const';
DOUBLE        : 'double';
ELSE          : 'else';
FLOAT         : 'float';
FUNCTION      : 'function';
IF            : 'if';
INT           : 'int';
LET           : 'let';
NOTHING       : 'nothing';
THEN          : 'then';

// § Integer Literals

IntegerLiteral
    :   DecimalIntegerLiteral
    ;

fragment
DecimalIntegerLiteral
    :   DecimalNumeral
    ;

fragment
DecimalNumeral
    :   '0'
    |   NonZeroDigit Digit*
    ;

fragment
NonZeroDigit
    :   [1-9]
    ;

fragment
Digit
    :   '0'
    |   NonZeroDigit
    ;

// § Boolean Literals

BooleanLiteral
    :   'true'
    |   'false'
    ;

// § Character Literals

CharacterLiteral
    :   '\'' SingleCharacter '\''
    |   '\'' EscapeSequence '\''
    ;

fragment
SingleCharacter
    :   ~['\\]
    ;

// § String Literals

StringLiteral
    :   '"' StringCharacters? '"'
    ;

fragment
StringCharacters
    :   StringCharacter+
    ;

fragment
StringCharacter
    :   ~["\\]
    |   EscapeSequence
    ;

// § Escape Sequences for Character and String Literals

fragment
EscapeSequence
    :   '\\' [btnfr"'\\]
    ;

fragment
ZeroToThree
    :   [0-3]
    ;

// § Separators

LPAREN          : '(';
RPAREN          : ')';
LBRACE          : '{';
RBRACE          : '}';
LBRACK          : '[';
RBRACK          : ']';
SEMI            : ';';
COMMA           : ',';
DOT             : '.';

// § Operators

ASSIGN          : '=';
GT              : '>';
LT              : '<';
BANG            : '!';
TILDE           : '~';
QUESTION        : '?';
COLON           : ':';
EQUAL           : '==';
LE              : '<=';
GE              : '>=';
NOTEQUAL        : '!=';
AND             : '&&';
OR              : '||';
INC             : '++';
DEC             : '--';
ADD             : '+';
SUB             : '-';
MUL             : '*';
DIV             : '/';
BITAND          : '&';
BITOR           : '|';
CARET           : '^';
MOD             : '%';
PLACEHOLDER     : '_';

ADD_ASSIGN      : '+=';
SUB_ASSIGN      : '-=';
MUL_ASSIGN      : '*=';
DIV_ASSIGN      : '/=';
AND_ASSIGN      : '&=';
OR_ASSIGN       : '|=';
XOR_ASSIGN      : '^=';
MOD_ASSIGN      : '%=';
LSHIFT_ASSIGN   : '<<=';
RSHIFT_ASSIGN   : '>>=';
URSHIFT_ASSIGN  : '>>>=';

// § Identifiers (must appear after all keywords in the grammar)

Identifier
    :   Letter LetterOrDigit*
    ;

fragment
Letter
    :   [a-zA-Z$_]
    |   ~[\u0000-\u00FF\uD800-\uDBFF]
    |   [\uD800-\uDBFF] [\uDC00-\uDFFF]
    ;

fragment
LetterOrDigit
    :   [a-zA-Z0-9$_]
    |   ~[\u0000-\u00FF\uD800-\uDBFF]
    |   [\uD800-\uDBFF] [\uDC00-\uDFFF]
    ;

fragment
Sign
    :   '+'
    |   '-'
    ;

AT : '@';
ELLIPSIS : '...';

//
// Whitespace and comments
//

WS  :  [ \t\r\n\u000C]+ -> skip
    ;

COMMENT
    :   '/*' .*? '*/' -> skip
    ;

LINE_COMMENT
    :   '//' ~[\r\n]* -> skip
    ;